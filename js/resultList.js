var resListMod = angular.module('resListMod', ['firebase', 'ngRoute', 'ngSanitize']);
	resListMod.controller('ResultListCtrl', ['$scope', '$http', '$location', '$routeParams', 'localStorageService', 'getResult', 'getRecipes', 'recipeCount', 'searchIngredients', 'searchType', 'searchCuisine', function($scope, $http, $location, $routeParams, localStorageService, getResult, getRecipes, recipeCount, searchIngredients, searchType, searchCuisine) {
		$scope.recipes = [];
		$scope.currentPageNr = $routeParams.nr;
		$scope.nrOfPages = 0;
		$scope.foodType = ["Breakfast", "Main Dish", "Side Dish", "Desserts", "Appetizers", "Salad"];
		$scope.cuisine = ["Asian", "Japanese", "American", "Italian", "English"];
		$scope.selectedItem;
		$scope.selectedCuisine;
		
		var nrOfRecipes = 0;
		var recipesPerPage = 25;
		var firstMenuDown = true;
		loadData($scope.currentPageNr);
		
		// Getter of data to show images and names of recipes
		function loadData(currentNr) {
			getRecipes.get(currentNr, recipesPerPage).then(function(data) {
				console.log(data);
				$scope.recipes = data;
				recipeCount.get().then(function(data) {
					nrOfRecipes = data;
					$scope.nrOfPages = Math.ceil(nrOfRecipes/recipesPerPage);
				});
			});
		};	

		// Returns a list of links with a range that doesn't go below or above the highest and lowest number of pages based on what the current page is
		$scope.pages = function() {
			var sizeOfPages = 5;
			var tempList = [];
			var first;

			if($scope.currentPageNr > 2) {
				first = $scope.currentPageNr-2;	
			}else {
				first = 1;
			}
			
			if($scope.nrOfPages < 5) {
				sizeOfPages = $scope.nrOfPages;
			}

			if(first > $scope.nrOfPages-sizeOfPages) {
				first = $scope.nrOfPages-sizeOfPages+1;
			}

			for (var i = first; i < first+sizeOfPages; i++) {
				tempList.push(i);
			};
			return tempList;
		};

		// Sets the current page-number of the list to the one received from the argument
		$scope.setPage = function(pageNr) {
			$scope.currentPageNr = pageNr;
		};	

		// Sets the id of the current recipe to be used in the recipeView 
		$scope.goTo = function(id){
			console.log(id);
			$location.path("/recipeView/"+id);
		};
		
		// Sets the current page to the previous one
		$scope.previous = function() {
			if($scope.currentPageNr > 1) {
				$scope.currentPageNr--;
				scroll(0, 0);	
			}
		};

		// Disables the previous-function if the current page is the first one
		$scope.prevDisabled = function(nr) {
			return nr === 1 ? "disabled" : "";
		};

		// Sets the current page to the next one 
		$scope.next = function() {
			if($scope.currentPageNr < $scope.nrOfPages) {
				$scope.currentPageNr++;
				scroll(0, 0);
			}
		};

		// Disables the next-function if the current page is the last one
		$scope.nextDisabled = function(nr) {
			return nr === $scope.nrOfPages ? "disabled" : "";
		};

		// Sets the background-image of the fifth star
		$scope.setStar5 = function(starNr) {
			var nr = Math.ceil(starNr);

			if(nr === 5) {
				return {
					backgroundImage:'url(img/yellowstar.png)'
				};
			}else {
				return {
					backgroundImage:'url(img/star.png)'
				};
			}
		};

		// Sets the background-image of the fourth star
		$scope.setStar4 = function(starNr) {
			var nr = Math.ceil(starNr);

			if(nr >= 4) {
				return {
					backgroundImage:'url(img/yellowstar.png)'
				};
			}else {
				return {
					backgroundImage:'url(img/star.png)'
				};
			}
		};

		// Sets the background-image of the third star
		$scope.setStar3 = function(starNr) {
			var nr = Math.ceil(starNr);

			if(nr >= 3) {
				return {
					backgroundImage:'url(img/yellowstar.png)'
				};
			}else {
				return {
					backgroundImage:'url(img/star.png)'
				};
			}
		};

		// Sets the background-image of the second star
		$scope.setStar2 = function(starNr) {
			var nr = Math.ceil(starNr);

			if(nr >= 2) {
				return {
					backgroundImage:'url(img/yellowstar.png)'
				};
			}else {
				return {
					backgroundImage:'url(img/star.png)'
				};
			}
		};

		// Sets the background-image of the first star
		$scope.setStar1 = function(starNr) {
			var nr = Math.ceil(starNr);

			if(nr >= 1) {
				return {
					backgroundImage:'url(img/yellowstar.png)'
				};
			}else {
				return {
					backgroundImage:'url(img/star.png)'
				};
			}
		};

		// Button to show or hide the filter
		$scope.filterArrow = function() {
			if($scope.arrowButton) {
				$scope.filterUp();	
			}else {
				$scope.filterDown();
			}
			$scope.arrowButton = !$scope.arrowButton;
			$scope.filterCheck = !$scope.filterCheck;
		};

		// Sets the selected type of meal in the filter + reload counter and list  
		$scope.selectItem = function(i){
			if($scope.selectedItem === i){
				$scope.selectedItem = null;
				searchType.type = null;
			}else{
				$scope.selectedItem = i;
				searchType.type = i;
			};
			counter();
			loadData($scope.currentPageNr);
		};

		// Sets the selected type of cuisine in the filter + reload counter and list 
		$scope.selectCuisine = function(i){
			if($scope.selectedCuisine === i){
		 		$scope.selectedCuisine = null;
		 		searchCuisine.cuisine = null;
			}else{
				$scope.selectedCuisine = i;
				searchCuisine.cuisine = i;
			};
			counter();
			loadData($scope.currentPageNr);
		};

		// Gets the current number of recipes in the list
		var counter = function(){
			recipeCount.get().then(function(data){
				$scope.counter = data;
			});
		};

		// Set the list to show the first page and to start on top of the view when filter is used
		$scope.filterUp = function(){
			$scope.currentPageNr = 1;
			scroll(0, 0);
		};
		
		// Gets the selected meal from the search view and shows that in the filter if it's not changed by user
		$scope.filterDown = function(){
			if(firstMenuDown) {
				$scope.selectItem(searchType.type);
				firstMenuDown = false;
			}
		};
	}]);		
		

